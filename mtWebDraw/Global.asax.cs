﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using mtTools;

namespace mtWebDraw
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current.IsCustomErrorEnabled)
                return;

            var httpException = new HttpException(null, Server.GetLastError());
            if (httpException.InnerException != null)
            {
                DebugHelper.WriteFile(Context.Request.UserHostAddress + "：" + httpException.InnerException.ToString(), "ApplicationError");
            }
        }
    }
}
