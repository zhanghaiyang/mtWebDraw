﻿Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
    onRender: function(ct, position) {
        this.el = ct.createChild({ tag: 'iframe', id: this.id, name: this.id, frameBorder: 0, scrolling: 'no', src: this.url });
    }
});
Ext.ux.ImgComponent = Ext.extend(Ext.BoxComponent, {
    onRender: function(ct, position) {
        this.el = ct.createChild({ tag: 'img', id: this.id, name: this.id, width: this.width, height: this.height, src: this.url });
    }
});
Ext.override(Ext.menu.Menu, {
    autoWidth: function() {
        var el = this.el, ul = this.ul;
        if (!el) {
            return;
        }
        var w = this.width;
        if (w) {
            el.setWidth(w);
        } else if (Ext.isIE && !Ext.isIE8) {
            el.setWidth(this.minWidth);
            var t = el.dom.offsetWidth;
            el.setWidth(ul.getWidth() + el.getFrameWidth("lr"));
        }
    }
});